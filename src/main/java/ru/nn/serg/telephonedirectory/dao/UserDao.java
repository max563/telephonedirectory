package ru.nn.serg.telephonedirectory.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;
import ru.nn.serg.telephonedirectory.utils.HibernateSessionFactoryUtil;

import java.util.List;
@Component
public class UserDao {

    public User findById(int id){
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(User.class, id);
    }

    public void save(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(user);
        tx1.commit();
        session.close();
    }
    public void savePhone(Phone phone) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(phone);
        tx1.commit();
        session.close();
    }

    public void update(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(user);
        tx1.commit();
        session.close();
    }

    //todo Метод не работает, при возможности - удалить
    public void delete(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
//        Session session = HibernateSessionFactoryUtil.getSessionFactory().getCurrentSession();
        Transaction tx1 = session.beginTransaction();
        List<Phone> phones = user.getPhoneList();
        for (Phone phone: phones){
            session.delete(phone);
        }
//        session.delete(user);
        tx1.commit();
        session.close();
    }

    public void delete(Integer id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        User user = session.find(User.class, id);
        session.remove(user);
        tx1.commit();
        session.close();
    }


    public Phone findPhoneById(int id) {
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(Phone.class, id);
    }

    public List<User> findAll() {
        return  HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("from User", User.class)
                .getResultList();
    }
}
