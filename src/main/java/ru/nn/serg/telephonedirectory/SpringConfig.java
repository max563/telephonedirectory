package ru.nn.serg.telephonedirectory;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("ru.nn.serg.telephonedirectory")
@PropertySource("classpath:TelephoneDirectory.properties")
public class SpringConfig {

}
