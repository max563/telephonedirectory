package ru.nn.serg.telephonedirectory;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.nn.serg.telephonedirectory.utils.Worker;
import ru.nn.serg.telephonedirectory.utils.SpringContextService;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(final String[] args) throws Exception {

        AnnotationConfigApplicationContext context= SpringContextService.getSpringContext();
        Worker worker = context.getBean(Worker.class);

        worker.showHelpMessage();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        boolean cycle = true;
        while (cycle) {
            String name = reader.readLine();
            switch (name) {
                case "lu":
                    worker.listUsers();
                    worker.showHelpMessage();
                    break;
                case "add":
                    worker.addUser();
                    worker.showHelpMessage();
                    break;
                case "del":
                    worker.deleteUser();
                    worker.showHelpMessage();
                    break;
                case "export":
                    worker.exportToXml();
                    worker.showHelpMessage();
                    break;
                case "h":
                    worker.showHelpMessage();
                    break;
                case "q":
                    cycle=false;
                    break;
                default:
                    break;
            }
        }
    }
}