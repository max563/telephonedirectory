package ru.nn.serg.telephonedirectory.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nn.serg.telephonedirectory.dao.Phone;
import ru.nn.serg.telephonedirectory.dao.User;
import ru.nn.serg.telephonedirectory.dao.UserDao;

import java.util.List;

@Component
public class UserService {

    @Autowired
    private UserDao usersDao;

    public UserService() {
    }

    /**
     * Возвращает юзера по его ID
     * @param id
     * @return
     */
    public User findUser(int id) {
        return usersDao.findById(id);
    }

    public void saveUser(User user) {
        usersDao.save(user);
    }

    public void savePhone(Phone phone) {
        usersDao.savePhone(phone);
    }

    public void deleteUser(User user) {
        usersDao.delete(user.getId());
    }

    public void updateUser(User user) {
        usersDao.update(user);
    }

    /**
     * Возвращает список пользователей
     * @return Список пользователей
     */
    public List<User> findAllUsers() {
        return usersDao.findAll();
    }

    public Phone findPhoneById(int id) {
        return usersDao.findPhoneById(id);
    }
}
