package ru.nn.serg.telephonedirectory.utils;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.nn.serg.telephonedirectory.SpringConfig;

public class SpringContextService {

    private static AnnotationConfigApplicationContext context;

    public static AnnotationConfigApplicationContext getSpringContext(){
        if (context==null){context=new AnnotationConfigApplicationContext(SpringConfig.class);}
        return context;
    }
}
