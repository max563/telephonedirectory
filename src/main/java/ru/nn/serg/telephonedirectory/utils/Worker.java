package ru.nn.serg.telephonedirectory.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.nn.serg.telephonedirectory.dao.Phone;
import ru.nn.serg.telephonedirectory.dao.User;
import ru.nn.serg.telephonedirectory.services.UserService;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

@Component
public class Worker {
    @Autowired
    UserService userService;

    @Value("${helpmessage}")
    private String helpmessage;
    @Value("${adduser_fio}")
    private String adduser_fio;
    @Value("${adduser_phone}")
    private String adduser_phone;
    @Value("${deleteuser_inputid}")
    private String deleteuser_inputid;
    @Value("${exportToXml_complete}")
    private String exportToXml_complete;

    private String getHelpMessage(){
        return helpmessage;
    }

    public void showHelpMessage(){
        System.out.println(getHelpMessage());
    }

    public void addUser() {
        String name = new String();
        String phones =new String();
        System.out.println(adduser_fio);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            name = reader.readLine();
        } catch (IOException e) {e.printStackTrace();}
        System.out.println(adduser_phone);
        try {
            phones = reader.readLine();
        } catch (IOException e) {e.printStackTrace();}

        User user=new User(name);

        for (String retval: phones.split(",")) {
            //del all escapes
            Phone phone=new Phone(retval.replaceAll(" ",""));
            phone.setUser(user);
            user.addPhone(phone);
        }
        userService.saveUser(user);
    }

    public void listUsers() {
        List<User> users=userService.findAllUsers();
        for(User user: users){
            List<Phone> phones = user.getPhoneList();
            System.out.print("ID:"+user.getId()+" Имя:"+user.getName());
            for (Phone phone:phones){
                System.out.print(" т."+phone.getPhoneNumber());
            }
            System.out.println("");
        }
    }

    /**
     * Раз Jackson использовать нельзя, напишем свой велосипед
     */
    public void exportToXml() {
        String tab="   ";
        try(FileWriter writer = new FileWriter("Contacts.xml", false))
        {
            // запись всей строки
            writer.write("<?xml version=\"1.0\"?>\r\n");
            writer.write("<list_of_contacts>\r\n");

            List<User> users=userService.findAllUsers();
            for(User user: users){
                List<Phone> phones = user.getPhoneList();
//                System.out.println(user.getId()+" "+user.getName());
                writer.write(tab+"<contact id=\""+user.getId()+"\">\r\n");
                writer.write(tab+tab+"<name>"+user.getName()+"</name>\r\n");
//                int idPhone=1;
                for (Phone phone:phones){
//                    System.out.println(phone.getPhoneNumber());
                    writer.write(tab+tab+"<phone id=\""+phone.getId()+"\">"+phone.getPhoneNumber()+"</phone>\r\n");
                }
                writer.write(tab+"</contact>\r\n");
            }

            writer.write("</list_of_contacts>\r\n");

            writer.flush();
            System.out.println(exportToXml_complete);
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    public void deleteUser() {
        Integer id=-1;
        System.out.println(deleteuser_inputid);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String str = reader.readLine();
            switch (str){
                case "q":
                    return;
                case "lu":
                    listUsers();
                    return;
                default:
                    id = Integer.parseInt(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*todo здесь бы при получении списка пользователей, организовать какой нибудь кэш надо
        чтобы в базу постоянно не лезть, либо сделать hql запрос в базу
         */
        List<User> users=userService.findAllUsers();
        for (User user:users){
            if(user.getId()==id & user.getId()>-1){
                userService.deleteUser(user);
            }
        }

    }
}
